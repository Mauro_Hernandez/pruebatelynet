package com.telynet.telynet.ui.Clientes

import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.telynet.telynet.R
import com.telynet.telynet.domain.Clientes.ListClientesUseCase
import com.telynet.telynet.models.Cliente
import com.telynet.telynet.ui.Clientes.AdapterListClientes.AdapterCardviewListClientes
import com.telynet.telynet.utils.CustomProgressBar
import com.telynet.telynet.viewModel.Clientes.ListClientesVieModelFactory
import com.telynet.telynet.viewModel.Clientes.ListClientesViewModel
import kotlinx.android.synthetic.main.fragment_list_clientes.*

class ListClientesFragment : Fragment() {
    private lateinit var viewModel: ListClientesViewModel
    private var recyclerView: RecyclerView?=null
    val progressBar = CustomProgressBar()
    val PHONE_REQUEST_CODE:Int =1


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view=inflater.inflate(R.layout.fragment_list_clientes, container, false)
        recyclerView=view.findViewById(R.id.rv_Clientes)
        setupInicioFragentViewModel()
        setObservableViewModel()

        return view!!
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupSearchsView()
    }

    fun setupSearchsView(){
        viewModel.getAllListClientes()
        filtrarPorVisitados()
        ordenarPorNombre()
        ordenarPorCodigo()
    }

    fun setupInicioFragentViewModel(){
        viewModel= ViewModelProviders.of(this,
            ListClientesVieModelFactory(
                ListClientesUseCase()
            )
        ).get(ListClientesViewModel::class.java)
        viewModel.attachView(this)
        //
    }

    fun setObservableViewModel(){
        val suggestObserver = Observer<List<Cliente>>{
            updateRecyclerView(it)
        }
        viewModel.getListArticlesLiveData().observe(this,suggestObserver)
    }

    fun updateRecyclerView(list: List<Cliente>){
        var adapter=
            AdapterCardviewListClientes(
                activity!!.applicationContext,
                list,
                this
            )
        recyclerView?.layoutManager= LinearLayoutManager(activity)
        recyclerView?.adapter=adapter
    }

    fun showProgressBar(){
        progressBar.show(activity!!,"Espera por favor...")
    }
    fun hideProgressBar(){
        progressBar.dialog.dismiss()
    }

    override fun onDetach() {
        super.onDetach()
        viewModel.dettachJob()
        viewModel.dettachView()
    }

    fun showException(message:String){
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()

    }
    fun filtrarPorVisitados(){
        cb_main_visitados.setOnClickListener {
            if(cb_main_visitados.isChecked){
                viewModel.getListClientesVisitados()
            }else{
                viewModel.getAllListClientes()
            }
        }
    }
    fun ordenarPorNombre(){
        btn_main_order_nombre.setOnClickListener {
            viewModel.ordenarPorNombre()
        }
    }
    fun ordenarPorCodigo(){
        btn_main_order_codigo.setOnClickListener {
            viewModel.ordenarPorCodigo()
        }
    }

    fun showMessageLyrics(message:String){
        Toast.makeText(activity, message, Toast.LENGTH_LONG).show()

    }


    var tel=""
    fun llamarCliente(telefono: String){
        try{
            tel=telefono
            val intent: Intent = Intent(Intent.ACTION_CALL)
            intent.data = Uri.parse("tel:${telefono.trim()}")
            if (ActivityCompat.checkSelfPermission(
                    activity!!,
                    android.Manifest.permission.CALL_PHONE
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                activity!!.startActivity(intent)
            } else {
                ActivityCompat.requestPermissions(activity!!,arrayOf(android.Manifest.permission.CALL_PHONE), PHONE_REQUEST_CODE )
            }


        }catch (e: Exception){
            showException(e.message.toString())

        }

        //viewModel.searchsLyrics(data)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if(requestCode==PHONE_REQUEST_CODE){
            if(grantResults.isNotEmpty() && grantResults[0]==PackageManager.PERMISSION_GRANTED){
                llamarCliente(tel)
            }
        }
    }


}
