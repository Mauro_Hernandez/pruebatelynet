package com.telynet.telynet.models

data class Cliente(
    var codigo: Int?=0,
    var nombre: String?="",
    var telefono: String?="",
    var email: String?="",
    var visitado: Boolean?=false
)