package com.telynet.telynet.domain.Clientes

import com.telynet.telynet.data.Clientes.ListClientesDataset
import com.telynet.telynet.models.Cliente

class ListClientesUseCase {
    private val articlesDataset= ListClientesDataset()

    fun getAllListClientes(): List<Cliente> {
        return articlesDataset.getAllListClientes()
    }
    fun getListClientesVisitados(): List<Cliente> {
        return articlesDataset.getListClientesVisitados()
    }

}