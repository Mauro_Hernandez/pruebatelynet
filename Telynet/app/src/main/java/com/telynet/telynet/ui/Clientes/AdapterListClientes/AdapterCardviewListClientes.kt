package com.telynet.telynet.ui.Clientes.AdapterListClientes

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.telynet.telynet.R
import com.telynet.telynet.ui.Clientes.ListClientesFragment
import com.telynet.telynet.models.Cliente
import kotlinx.android.synthetic.main.cardview_cliente.view.*

class AdapterCardviewListClientes(var context: Context, listClientes:List<Cliente>, view: ListClientesFragment): RecyclerView.Adapter<AdapterCardviewListClientes.AdapterViewHolder>() {
    var view: ListClientesFragment?=view
    var listClientes:List<Cliente>?=listClientes
    var viewHolder: AdapterViewHolder?=null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterViewHolder {
        val v=LayoutInflater.from(parent?.context).inflate(R.layout.cardview_cliente,parent,false)
        viewHolder=
            AdapterViewHolder(
                v
            )
        return viewHolder!!

    }

    override fun getItemCount(): Int {
        return listClientes?.size!!
    }

    override fun onBindViewHolder(holder: AdapterViewHolder, position: Int) {
        holder.codigo.text=listClientes?.get(position)?.codigo.toString()!!
        holder.nombre.text=listClientes?.get(position)?.nombre!!
        holder.email.text=listClientes?.get(position)?.email!!
        holder.tel.text=listClientes?.get(position)?.telefono!!
        if(listClientes?.get(position)?.visitado!!){
            holder.visi.isChecked=true
        }
        holder.tel.setOnClickListener {
            view?.llamarCliente(listClientes?.get(position)?.telefono!!)
        }
    }

    class AdapterViewHolder(view:View):RecyclerView.ViewHolder(view!!){
        val codigo=view.tv_card_codigo
        val nombre=view.tv_card_name
        val email=view.tv_card_email
        val tel=view.tv_card_tel
        val visi=view.cb_card_visitado

    }
}