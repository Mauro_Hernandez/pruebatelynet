package com.telynet.telynet.viewModel.Clientes

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.telynet.telynet.domain.Clientes.ListClientesUseCase

class ListClientesVieModelFactory(val listClientesUsecase: ListClientesUseCase):ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return modelClass.getConstructor(ListClientesUseCase::class.java).newInstance(listClientesUsecase)
    }
}