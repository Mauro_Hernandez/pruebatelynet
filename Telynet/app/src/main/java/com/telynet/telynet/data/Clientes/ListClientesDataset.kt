package com.telynet.telynet.data.Clientes

import com.telynet.telynet.models.Cliente


class ListClientesDataset {


    val listCliente:MutableList<Cliente>?= mutableListOf(
        com.telynet.telynet.models.Cliente(1, "Mauro", "5555555555","a@telynet",true),
        com.telynet.telynet.models.Cliente(2, "Laura", "6666666666","a@telynet",true),
        com.telynet.telynet.models.Cliente(3, "Carla", "7777777777","a@telynet",true),
        com.telynet.telynet.models.Cliente(4, "Carlos", "888888888","a@telynet",false),
        com.telynet.telynet.models.Cliente(5, "Diego", "9999999999","a@telynet",true),
        com.telynet.telynet.models.Cliente(6, "Fernando", "222222222","a@telynet",false),
        com.telynet.telynet.models.Cliente(7, "Paul", "33333333333","a@telynet",true),
        com.telynet.telynet.models.Cliente(8, "Esther", "4444444444","a@telynet",false)
    )

    fun getAllListClientes(): List<Cliente> {
        return listCliente!!
    }
    fun getListClientesVisitados(): List<Cliente> {
        val filtered = listCliente!!.filter { it.visitado==true }
        return filtered!!
    }


}