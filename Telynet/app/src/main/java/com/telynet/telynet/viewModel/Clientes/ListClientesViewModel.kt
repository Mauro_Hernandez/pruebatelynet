package com.telynet.telynet.viewModel.Clientes

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.telynet.telynet.domain.Clientes.ListClientesUseCase
import com.telynet.telynet.exceptions.Exceptions
import com.telynet.telynet.models.Cliente
import com.telynet.telynet.ui.Clientes.ListClientesFragment
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

class ListClientesViewModel(val listClientesUseCase: ListClientesUseCase):ViewModel(), CoroutineScope {
    private var view: ListClientesFragment?=null
    private val listSuggest= MutableLiveData<List<Cliente>>()
    private var listClientes:List<Cliente>?= listOf()
    val job=Job()

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main+job


    fun getAllListClientes(){
        launch{
            try{
                view?.showProgressBar()
                listClientes=listClientesUseCase.getAllListClientes()
                setListSuggest(listClientes!!)
                view?.hideProgressBar()
            }catch (e: Exceptions){
                view?.showException(e.message.toString())
                view?.hideProgressBar()
            }
        }

    }
    fun getListClientesVisitados(){
        launch{
            try{
                view?.showProgressBar()
                listClientes=listClientesUseCase.getListClientesVisitados()
                setListSuggest(listClientes!!)
                view?.hideProgressBar()
            }catch (e: Exceptions){
                view?.showException(e.message.toString())
                view?.hideProgressBar()
            }
        }

    }
    fun ordenarPorNombre(){

            try{
                view?.showProgressBar()
                setListSuggest(listClientes!!.sortedWith(compareBy({ it.nombre })))
                view?.hideProgressBar()
            }catch (e: Exceptions){
                view?.showException(e.message.toString())
                view?.hideProgressBar()
            }


    }
    fun ordenarPorCodigo(){

            try{
                view?.showProgressBar()
                setListSuggest(listClientes!!.sortedBy{it.codigo})
                view?.hideProgressBar()
            }catch (e: Exceptions){
                view?.showException(e.message.toString())
                view?.hideProgressBar()
            }


    }



    fun setListSuggest(listArticles: List<Cliente>){
        this.listSuggest.value=listArticles
    }

    fun getListArticlesLiveData(): LiveData<List<Cliente>> {
        return listSuggest
    }

    fun attachView(view: ListClientesFragment){
        this.view=view
    }
    fun dettachView(){
        this.view=null
    }

    fun dettachJob(){
        coroutineContext.cancel()
    }
}